

readme:: readme.xfm always
	# {X}fm insists on a single space indention even when indent == 0i; must be chopped
	XFM_LIB=$${XFM_LIB:-/usr/local/share/xfm} MARKDOWN=1 tfm readme.xfm - | sed 's/^ //' >README.md
	XFM_LIB=$${XFM_LIB:-/usr/local/share/xfm} MARKDOWN=0 tfm readme.xfm README

# make hack; readme is alawys out of date
always::
