
package main

import (
	"fmt"
)

func main( ) {

	count := 0
	data_ch := make( chan []byte, 1024 )		// allow 1k buffers to queue on this channel
	go Listen_udp( 4567, data_ch )					// start the litener

	for ;; {
		buf := <- data_ch
		fmt.Printf( "(%s)\n", string( buf ) )
		count++
		if count > 100 {
			break
		}
	}
}
