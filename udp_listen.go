/*
	Abstract:	This module contains listeners started as a go routine.
				The UDP listener creates a connection manager (cmgr)
				and expects updates from UDP messages. The bus listener
				uses busdriver to listen for updates on the message bus.

	Date:		28 August 2018
				2 May 2024 (bus listener added)
	Author:	E. Scott Daniels
*/

package main

import (
	"bytes"
	"fmt"
	"os"

	"bitbucket.org/EScottDaniels/cmgr"
	"gitlab.com/rouxware/busdriver"
)

/*
	Create a busdriver to interface with the message bus. We expect it to push
	messages onto our channel and we will in turn push those messages onto
	the data channel passed here. This function blocks, so start as a goroutine
	or as the last call in the "stack."
*/
func ListenBus( marquee string, busAddr string, dataChan chan []byte ) {
	bd, err := busdriver.NewBusDriver( "busstop", busdriver.TT_Nats, busAddr )
	if err != nil {
		fmt.Fprintf( os.Stderr, "## CRASH ## unable to allocate a busdriver: %s\n", err )
		os.Exit( 1 )
	}

	mChan := make( chan *busdriver.Message, 2048 )		// chan we listen to the bus on
	err = bd.ListenFor( mChan, marquee )
	if err != nil {
		fmt.Fprintf( os.Stderr, "## CRASH ## unable to set busdriver listen for: %s: %s\n", marquee, err )
		os.Exit( 1 )
	}

	for {
		bMsg := <- mChan        // hang out until a message arrives
		if bMsg != nil {			// it could be, take no chances
			dataChan <- bMsg.Rider	// dump the payload into the channel
		}
	}
}

/*
	Create a connection manager object and have it listen on the indicated port for UDP
	packets. Packets are combined until a newline is seen at which point the buffer
	(without the newline) is written on the channel. Escaped newlines (e.g. \<nl>) are
	recognised and removed from the stream (a blank is inserted in its place). The max
	buffer size supported is 4k.
*/
func Listen_udp( port int, usr_chan chan []byte  ) {

	data_chan := make( chan *cmgr.Sess_data, 1024 );	// serial (0) so 'sender' will block until we read it

	smp := cmgr.NewManager( "", data_chan );			// create a connection manager object, empty port string prevents TCP listener from starting
	smp.Listen_udp( port, data_chan )

	buf := bytes.NewBuffer( make( []byte, 4096 ) )
	buf_size := 0

	for {											// wait for packets
		if sdp := <- data_chan; sdp != nil {		// odd, but sdp MUST be := declared
			switch( sdp.State ) {
				case cmgr.ST_ACCEPTED:		// new accepted connection
					fmt.Fprintf( os.Stderr, "unexpected TCP session accepted? HUH?\n" )

				case cmgr.ST_NEW:		// new connection
					fmt.Fprintf( os.Stderr, "unexpected new TCP session? HUH?\n" )

				case cmgr.ST_DISC:

				case cmgr.ST_DATA:
					//fmt.Fprintf( os.Stderr, "received data: (%s)\n", sdp.Buf )
					data := sdp.Buf
					for ; data != nil && len( data ) > 0; {

						i := bytes.IndexByte( data, byte( '\n' ) )		// newline in there?
						if i >= 0 {
							if i > 0  && data[i-1] == '\\' {	// escaped newline
								buf.Write( data[0:i-1] )
								buf_size += i - 1
								if i + 1 < len( data ) {
									data = data[i+1:]
								} else {
									break
								}
							} else {
								if buf_size > 0 {
									buf.Write( data[0:i] )		// write, trimming newline
									usr_chan <- buf.Bytes()		// send it along
									buf = bytes.NewBuffer( make( []byte, 4096 ) )
									buf_size = 0
								} else {
									usr_chan <- data[0:i]
								}

								if i + 1 < len( data ) {
									data = data[i+1:]
								} else {
									break
								}
							}
						} else {
							buf.Write( data )
							buf_size += len( data )
							break
						}
					}
			}
		}
	}
}

