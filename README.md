          
 
 
[//]: # ( 
WARNING! This file is generated from an {X}fm source file. Do 
NOT modify it in place; modify the source and rebuild it 
) 
 
 
 
 
# W1KW 
They say a picture is worth a thousand words; get it now? 
 
A view is a description of one or more drawing elements which is 
loaded and managed by W1kW which listens on a well known port 
for UDP messages, and updates the view with changes to the 
elements themselves (position, size etc.) or their data (graph, 
meter, text values). 
 
W1kW depends on the sketch drawing interface (also bitbucket) 
and underlying X window support to allow Go applications to 
create and manipulate an Xwindow. 
 
This is a work in progress. Future effort will include the 
addition of loading different sub/views dynamically, as well as 
scaling and other nice to haves. Also, some work with respect to 
tuning so that CPU usage is lowered; right now meter and graph 
animation causes the process to be somewhat expensive; about 5% 
utilisation for 4 bar/run graphs and 7 meters with a repaint 
frequency of .1 second. Using a longer update period (.5s 
reduces this to < 1%. 
