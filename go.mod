module bitbucket.org/EScottDaniels/w1kw

go 1.21.5

require (
	bitbucket.org/EScottDaniels/cmgr v1.1.0
	bitbucket.org/EScottDaniels/sketch v1.2.0
	bitbucket.org/EScottDaniels/view v1.3.0
	gitlab.com/rouxware/busdriver v1.1.0
)

require (
	bitbucket.org/EScottDaniels/colour v1.2.0 // indirect
	bitbucket.org/EScottDaniels/data_mgr v1.1.0 // indirect
	bitbucket.org/EScottDaniels/drawingthings v1.3.1 // indirect
	bitbucket.org/EScottDaniels/menu v1.2.0 // indirect
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/nats-io/nats.go v1.37.0 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	gitlab.com/rouxware/goutils/clike v1.0.0 // indirect
	gitlab.com/rouxware/goutils/token v1.0.0 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
