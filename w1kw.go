//:vi ts=4 sw=4 noet:

/*
	Mnemonic:	w1kw.go Revised
	Abstract:	This is the main for w1kw -- worth 1000 words.
	Author:		E. Scott Daniels
	Date:		29 August 2018
	Revision:	25 June 2023
*/

package main

import (
	"fmt"
	"flag"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
	"bitbucket.org/EScottDaniels/view"
)

// ---------------- utilities ------------------------------
func atof( buf string ) float64 {
	val, err := strconv.ParseFloat( buf, 64 )
	if err != nil {
		return 0.0
	}

	return val
}

/*
	The thing on which we prop our work.
*/
type Easel struct {
	sync.Mutex
	gc sketch.Graphics_api	// the current graphics context
	vw *view.View			// the current view being displayed
	scale float64
	paintDelay int			// milliseconds between paint operations
	driverDelay int		// milliseconds between mouse driver calls

}

/*
	Simple loop to force a repaint fairly frequently so that animated things (meters) will
	function.
	Show_factor is the divisor applied to the delay and used to further limit the frequency
	that all views are redisplayed.  Typically the drive() need is much higher (10 ms or so)
	but the need to paint for most applications can be 10x slower, so the factor would be
	10.  -D sets the factor on the command line. The smaller the factor, the more CPU the
	application will use as it will redisplay more frequently.

*/
func (ep *Easel) Painter() {
	pTimer := time.NewTicker( time.Duration( ep.paintDelay ) * time.Millisecond )	// pop for painting
	mTimer := time.NewTicker( time.Duration( ep.driverDelay ) * time.Millisecond )  // pop for polling

	for {
		select {
			case <- pTimer.C:
				ep.Lock()
				ep.vw.Paint( ep.gc, ep.scale )
				ep.gc.Show()					// update display
				ep.Unlock()

			case <- mTimer.C:
				ep.Lock()
				ep.gc.Drive()
				ep.Unlock()
		}
	}
}

func abs( x int ) int {
	if x < 0 {
		return -x
	}

	return x
}

/*
	If no buttons are activated by the view, this function will receive click events.
	When an event is received, the view's hard click function is driven.

	Left button events are intercepted as follows:
		press always records the mouse location

		release translates the view if the mouse was moved, otherwise
		the release is passed to the view.
*/
func (ep *Easel) Clicker( inch chan *sktools.Interaction ) {
	pressX := 0  // x,y of pointer when middle button pressed
	pressY := 0
	deltaX := 0
	deltaY := 0

	for ;; {
		e := <- inch

		if e.Data == 1 { // left button
			switch e.Kind {
				case sktools.IK_M_RELEASE:
					dX :=  int( e.X * (1.0/ep.scale) ) - pressX
					dY :=  int( e.Y * (1.0/ep.scale) ) - pressY
					if abs( dX ) > 5 || abs( dY ) > 5  {
						deltaX += dX
						deltaY += dY

						ep.Lock()
						ep.gc.Translate_delta( float64( dX  ), float64( dY ) )
						ep.Unlock()
						continue
					}

				case sktools.IK_M_PRESS:
					pressX = int( e.X * (1.0/ep.scale) )
					pressY = int( e.Y * (1.0/ep.scale) )
					continue

				default:
					continue
			}
		}

		e.X -= float64(deltaX)  // must adjust by the amount we have moved things round
		e.Y -= float64(deltaY)
		ep.vw.Hard_click( e )	// pass to view if no motion
	}
}

func main( ) {

	fmt.Fprintf( os.Stderr, "[INFO] w1kw2 2.1 started\n" )

	busAddr := flag.String( "bus", "", "message bus server address(es) (comma sep host:port)" )
	view_file := flag.String( "f", "main.view", "view file name" )
	paint_delay := flag.Int( "d", 10, "delay between paints (milliseconds)" )
	driver_delay := flag.Int( "D", 10, "delay between mouse driver polls (milliseconds)" )
	wheight := flag.Int( "h", 650, "window height" )
	marquee := flag.String( "marquee", "w1kw.update", "message bus marquee to listen to" )
	port := flag.Int( "p", 6543, "UDP listen port" )
	scale := flag.Float64( "s", 1.0, "Drawing scale" )
	wtitle := flag.String( "t", "", "window title" )
	wwidth := flag.Int( "w", 400, "window width" )
	flag.Parse()

	ep := &Easel{
		scale: *scale,
		driverDelay: *driver_delay,
		paintDelay: *paint_delay,
	}

	if *wtitle == "" {
		t := fmt.Sprintf( "W1kW: %s", *view_file )
		wtitle = &t
	}

	data_ch := make( chan []byte, 1024 )		// allow 1k buffers to queue on this channel
	if *busAddr != "" {
		go ListenBus( *marquee, *busAddr, data_ch )		// listen for updates on the message bus
	} else {
		go Listen_udp( *port, data_ch )				// start the litener
	}

	var err error
	ep.vw, err = view.MkScaledView( *view_file, *scale )	// some things like graphs need to know scale
	if err != nil {											// stderr message already written by view function
		os.Exit( 1 )
	}

	ep.gc, err = sketch.Mk_graphics_api( "xi", fmt.Sprintf( ",%d,%d,%s", *wheight, *wwidth, *wtitle ) )
	if err != nil {
		fmt.Fprintf( os.Stderr, "unable to open graphics context: %s\n", *view_file, err )
		os.Exit( 1 )
	}
	ep.vw.Set_scale( *scale )
	ep.gc.Set_scale( *scale, *scale )

	click_ch := make( chan *sktools.Interaction, 25 )
	go ep.Clicker( click_ch )

	ep.gc.Add_listener( click_ch )		// listen for updates on UDP

	go ep.Painter( )	// drive mouse poll and painting

	for ;; {  // finally, wait for events from the listener and act on them
		bbuf := <- data_ch

		buf := string( bbuf )
		tokens := strings.SplitN( buf, " ", 3 )  // <msg-type> <name> <data>

		switch tokens[0] {
			// -- view oriented requests ---------------------------------------
			case "data_update":
				if len( tokens ) == 3 {
					ep.Lock()
					ep.vw.Data_update( tokens[1], tokens[2] )
					ep.Unlock()
				} else {
					fmt.Fprintf( os.Stderr, "bad request missing tokens: %s\n", tokens[0] )
				}

			case "update":
				ep.Lock()
				ep.vw.Update( buf );
				ep.Unlock()

			// -- w1kw  oriented requests ---------------------------------------
			case "load":	// pull in a new view and replace on success
				if len( tokens ) != 2 {
					continue
				}
				newView, err := view.MkScaledView( tokens[1], *scale )
				if err == nil {
					ep.Lock()
					ep.vw = newView
					ep.Unlock()
				}

			case "scale": // set a hard scale in the current view
				if len( tokens ) != 2 {
					continue
				}
				newScale := atof( tokens[1] )
				if newScale > 0.0 && newScale <= 1000.0 {
					ep.Lock()
					ep.scale = newScale
					ep.gc.Set_scale( newScale, newScale )
					ep.vw.Set_scale( newScale ) // view must capture scale to scale x,y when mouse clicks received
					ep.Unlock()
				}

			default:
				//fmt.Fprintf( os.Stderr, "unknown request: %s\n", tokens[0] )
		}
	}
}
