#!/usr/bin/env ksh

# Mnemonic:	build.ksh
# Abstract:	This script builds various things based on $1:
#					udp_test -- build a simple udp listener with udp_listen_test.go
#					w1kw     -- The w1kw binary
#
#			The assumption is that the underlying XI library and header file (libxi.a and
#			xi.h) are installed in /usr/local/lib and /usr/local/include.  However, if 
#			building in a development environment where the desired instance of XI is in 
#			the source tree, the necessary environment variables can be placed into ./setup
#			and will be included by this script.  For example:
#
#
#				# local setup before building
#				export GOROOT=/usr2/build/go                
#				export LIBRARY_PATH=$HOME/src/xi
# Date:		20 June 2018
# Author:	E. Scott Daniels
#
# Mods:		01 Jan 2019 - add setup and findlib support.
#-------------------------------------------------------------------------------------------

function find_lib {
	found=0
	for d in /usr/include /usr/local/include $C_INCLUDE_PATH
	do
		if [[ -f $d/xi/xi.h ]]
		then
			found=1
			break
		fi
	done

	if (( ! found ))
	then
		echo "build.ksh: cannot find include directory with xi.h; consider setting C_INCLUDE_PATH"
		exit
	fi

	for d in /usr/lib /usr/local/lib ${LD_LIBRARY_PATH//:/ }
	do
		if [[ -f $d/libxi.a ]]
		then
			return
		fi
	done
	
	echo "cannot find libxi.a in system lib directories or LD_LIBRARY_PATH"
	exit 1
}

find_lib 

case ${1:-main} in 
	static)	
			export CGO_ENABLED=1
			rm -f w1kw
			# static requires that ALL X libraries be present which is hard in a container
			if go build -ldflags="-L /lib64  -extldflags=-static" w1kw.go udp_listen.go 
			then
				echo "static main built"
			fi
			;;

	arm)
			rm -f w1kw
			# these are needed for go
			export GOARCH=arm64
			export GOOS=linux
			export PATH=$PATH:/usr/lib/go-1.20/bin

			# odd that both need to be set, but they do
			export CC_FOR_TARGET=aarch64-linux-gnu-gcc-12
			export CC=aarch64-linux-gnu-gcc-12

			export CGO_ENABLED=1
			export CGO_CFLAGS=" -I /data/arm_root/include/xi"
			#CGO_CFLAGS=""
			export CGO_LDFLAGS="-L/data/arm_root/lib"
			go build -tags no_pkgconfig w1kw.go udp_listen.go
			;;
		
	w1kw|main)	
			export CGO_ENABLED=1
			rm -f w1kw
			# static requires that ALL X libraries be present which is hard in a container
			#if go build -ldflags="-extldflags=-static" w1kw.go udp_listen.go 
			if go build w1kw.go udp_listen.go 
			then
				echo "main built"
			fi
			;;

	udp*)	if go build udp_listen*.go
			then
				echo "udp_listen built"
			fi
			;;
esac
